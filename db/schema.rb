# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180109184212) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "books", force: :cascade do |t|
    t.string "title"
    t.integer "publisher_id"
    t.integer "copies_in_stock"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "books_checkouts", id: false, force: :cascade do |t|
    t.bigint "checkout_id"
    t.bigint "book_id"
    t.integer "quantity"
    t.index ["book_id"], name: "index_books_checkouts_on_book_id"
    t.index ["checkout_id"], name: "index_books_checkouts_on_checkout_id"
  end

  create_table "books_shops", id: false, force: :cascade do |t|
    t.bigint "shop_id"
    t.bigint "book_id"
    t.index ["book_id"], name: "index_books_shops_on_book_id"
    t.index ["shop_id"], name: "index_books_shops_on_shop_id"
  end

  create_table "checkouts", force: :cascade do |t|
    t.integer "shop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "publishers", force: :cascade do |t|
  end

  create_table "shops", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
