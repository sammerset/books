class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string  :title
      t.integer :publisher_id
      t.integer :copies_in_stock

      t.timestamps
    end
  end
end
