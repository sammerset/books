class CreateShops < ActiveRecord::Migration[5.1]
  def change
    create_table :shops do |t|
      t.string  :name

      t.timestamps
    end

    create_table :books_shops, id: false do |t|
      t.belongs_to :shop, index: true
      t.belongs_to :book, index: true
    end
  end
end
