class CreateCheckouts < ActiveRecord::Migration[5.1]
  def change
    create_table :checkouts do |t|
      t.integer :shop_id

      t.timestamps
    end

    create_table :books_checkouts, id: false do |t|
      t.belongs_to :checkout, index: true
      t.belongs_to :book, index: true
      t.integer    :quantity
    end    
  end
end
