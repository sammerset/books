# README
Ruby 2.4.2
Postgresql

$ git clone git@github.com:sammerset/books.git
$ cd books
$ gem install bundler
$ bundle install
$ rake db:create && db:migrate
$ rails s -p 3000

rails console

p1 = Publisher.create
p2 = Publisher.create

s1 = Shop.create(name: 'amazon')
s2 = Shop.create(name: 'alibaba')

b1 = Book.create(publisher: p1, title: '3 Kings', copies_in_stock: 100)
b2 = Book.create(publisher: p2, title: '3 Queens', copies_in_stock: 200)

s1.books << [b1, b2]
s2.books << [b1, b2]

REQUESTS EXAMPLES:

POINT 1
GET Shops: curl localhost:3000/api/v1/publishers/{PUBLISHER_ID}/shops

POINT 2
POST Checkout: curl --request POST localhost:3000/api/v1/checkouts --data "checkout[shop_id]={SHOP_ID}&checkout[books_checkouts_attributes][0][book_id]={BOOK_ID}&checkout[books_checkouts_attributes][0][quantity]={QUANTITY}"