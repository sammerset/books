class Shop < ApplicationRecord
  has_and_belongs_to_many :books
  has_many :checkouts

  validates :name, presence: true

  def self.with_books_in_stock_for_publisher(publisher)
    select([ :id,
             'SUM(books_checkouts.quantity) as books_sold_count',
             'MAX(name) as name',
             'ARRAY_AGG(DISTINCT(books.id)) as books_ids' ])
      .left_joins( books: { books_checkouts: :checkout } )
      .where( books: { publisher: publisher } )
      .where( 'copies_in_stock > 0' )
      .group(:id)
      .order('SUM(books_checkouts.quantity)')
  end
end
