class Checkout < ApplicationRecord
  has_and_belongs_to_many  :books
  has_many                 :books_checkouts, class_name: :BooksCheckouts
  belongs_to               :shop

  accepts_nested_attributes_for :books_checkouts, reject_if: proc { |a| a['quantity'].blank? }

  validate :enough_copies, :books_presence, on: :create

  def enough_copies
    if books_checkouts.any?{ |f| !f.book.has_enough_copies_for_sale?(f.quantity) }
      errors.add(:books, 'not enough copies for sale')
    end
  end

  def books_presence
    unless books_checkouts.any?
      errors.add(:books, 'not specified for order')
    end
  end

  def self.create(attributes)
    begin
      transaction(isolation: :serializable) do
        super
      end
    rescue => e
      Rails.logger.info "Checkout creation exception: #{e}"
      (checkout = Checkout.new).errors.add(:id, e)
      checkout
    end
  end
end
