class Book < ApplicationRecord
  belongs_to :publisher
  has_many   :books_checkouts, class_name: :BooksCheckouts

  validates :title, presence: true
  validates :copies_in_stock, numericality: true

  def self.books_in_stock(ids)
    select([ :id,
             'MAX(title)',
             '(MAX(copies_in_stock) - COALESCE(SUM(quantity), 0)) as copies_in_stock' ])
      .left_joins(:books_checkouts)
      .where(id: ids)
      .group(:id)
      .having('COALESCE(SUM(quantity), 0) > 0')
  end

  def has_enough_copies_for_sale?(quantity)
    Book
      .select('(MAX(copies_in_stock) - COALESCE(SUM(quantity), 0)) as solded_quantity')
      .left_joins(:books_checkouts)
      .where(id: id)
      .group(:id)
      .first
      .solded_quantity > quantity
  end
end
