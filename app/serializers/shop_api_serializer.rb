class ShopApiSerializer < ActiveModel::Serializer
  attributes :id,
             :name

  attribute :books_sold_count do
    object.books_sold_count || 0
  end

  attribute :books_in_stock do
    Book.books_in_stock(object.books_ids)
  end
end
