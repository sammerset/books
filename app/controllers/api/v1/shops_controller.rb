module Api
  module V1
    class ShopsController < Api::V1::BaseController
      def index
        publisher = Publisher.where(id: params[:publisher_id])

        render json: Shop
          .with_books_in_stock_for_publisher(publisher),
          each_serializer:  ShopApiSerializer
      end
    end
  end
end
