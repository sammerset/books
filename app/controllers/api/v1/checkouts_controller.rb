module Api
  module V1
    class CheckoutsController < Api::V1::BaseController
      def create
        checkout = Checkout.create(checkout_params)
        render json: { success: !checkout.errors.any?,
                       errors: checkout.errors.full_messages.presence }
      end

      def checkout_params
        params
          .require(:checkout)
          .permit( :shop_id,
                   books_checkouts_attributes: [
                     :book_id,
                     :quantity
                   ] )
      end
    end
  end
end
